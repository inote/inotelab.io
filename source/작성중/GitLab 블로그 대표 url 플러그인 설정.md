---
title: GitLab 블로그 대표 url 플러그인 설정
date: 2017-11-18 24:00:00
tags: GitLab
categories:
  - GitLab
---

GitLab 블로그의 기본 Hexo에는 대표 url 설정이 없습니다. 플러그인을 추가해 주어야 합니다.
hexo가 설치된 폴더에서 다음 커맨드를 입력하여 npm 모듈을 설치합니다.
```
$ npm install --save hexo-auto-canonical
```
설치가 끝나면 <head> 태그 안에 자동으로 대표url이 들어가도록 코드를 삽입해야 합니다.
head.ejs 파일을 열어서 <%- meta(page) %> 의 아래에 
<%- autoCanonical(config, page) %> 코드를 삽입합니다.

```
\inote.gitlab.io\themes\hueman\layout\common\head.ejs
```