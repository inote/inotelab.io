---
title: RSS feed 만들기
date: 2017-11-17 24:00:00
tags: GitLab
categories:
  - GitLab
---

RSS feed 자동 생성 플러그인 설치

```
$ npm install hexo-generator-feed --save
```

_config.yml 파일 수정

```
feed:
  type: atom
  path: feed.xml
  limit: 20
  hub:
```