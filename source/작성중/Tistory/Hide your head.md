
[원문](http://wicky.`nillia`.ms/headroom.js/)



[Headroom.js](http://wicky.nillia.ms/headroom.js/) 사이트에서 `headroom.min.js`파일을 다운로드 받습니다.

`headroom.min.js`파일을 티스토리의 이미지 폴더에 업로드 합니다.

아니면 [외부사이트](https://cdnjs.com/libraries/headroom)의 링크를 써도 됩니다.

1. 스크립트를 `html`에 추가합니다.
```
<script src="https://cdnjs.cloudflare.com/ajax/libs/headroom/0.9.4/headroom.min.js">
```

2. 헤더 부분에 아래 클래스를 추가함.
`dkHead` 부분을 `header`로 수정

```
<!-- initially -->
<header class="headroom">

<!-- scrolling down -->
<header class="headroom headroom--unpinned">

<!-- scrolling up -->
<header class="headroom headroom--pinned">
```

3. css 수정

```
.headroom--pinned {
    display: block;
}
.headroom--unpinned {
    display: none;
}
```