### Hexo Tag Plugins

Hexo 자체적인 포스트를 쓰는 문법을 Tag Plugin이라 합니다.

### Block Quote

```
{% blockquote [author[, source]] [link] [source_link_title] %}
content
{% endblockquote %}
```

### 책 인용 (저자 + 출처 제목)

```
{% blockquote Amelie Nothomb, le voyage d'hiver %}
사랑에는 실패가 없다.
{% endblockquote %}
```

### 트위터 인용 (저자 + 출처 링크)

트위터 내용은 @npmdaily 가 올린 npm package 중 하나로 Hexo 의 Keycap 추가하는 플러그인입니다.

```
{% blockquote @npmdaily https://twitter.com/npmdaily/status/743858563299311616 %}
hexo-tag-kbd - Displays the keycaps in your hexo post/page. http://npmdaily.com/pkg/hexo-tag-kbd …  #npm #javascript #nodejs
{% endblockquote %}
```

### 웹 페이지 인용 (저자 + 출처 링크 + 출처 제목)

```
{% blockquote Eric Han http://futurecreator.github.io/2016/06/14/get-started-with-hexo/ 워드프레스보다 쉬운 Hexo 블로그 시작하기%}
Hexo는 github pages를 이용한 블로그입니다. Github Pages 는 github 유저와 프로젝트의 정적인(static) 홈페이지를 자동으로 만들어주고 github.io 도메인으로 호스팅해주는 서비스입니다. 즉, 서버의 내용을 github 에 push만 하면 실시간 적용됩니다. 아주 간단하죠?
{% endblockquote %}
```

### 코드 삽입

아무래도 IT 블로그를 하다보면 소스코드를 많이 추가하게 됩니다. Hexo 에서는 여러가지 기능을 제공합니다. 소스의 제목, 언어, url, 링크 제목을 옵션으로 표시할 수 있습니다. 그리고 Hexo 는 highlight.js 를 사용해서 소스코드를 표시하기 때문에 highlight.js 를 이용하면 다양한 커스터마이징도 가능합니다.

```
{% codeblock [title] [lang:language] [url] [link text] %}
code snippet
{% endcodeblock %}
```

캡션 추가하기

예시처럼 파일명을 명시할 수 있겠네요.
```
{% codeblock Array.map %}
array.map(callback[, thisArg])
{% endcodeblock %}
```

### 마크다운 형식의 코드 블락

마크다운 형식의 코드 블락 했을 때도 제목이나 링크 지정할 수 있었군요. 이렇게 쓰는게 제일 낫겠습니다.

```
``` [language] [title] [url] [link text] code snippet ```
```

### jsFiddle

jsFiddle 은 온라인 상에서 HTML, CSS, javaScript 를 작성하고 테스트할 수 있는 서비스입니다.
```
{% jsfiddle shorttag [tabs] [skin] [width] [height] %}
```

- shorttag: jsFiddle 에서 코드를 저장하면 대시보드에서 확인할 수 있는 코드 이름입니다. 그걸 입력하시면 알아서 불러옵니다.
- skin: theme 가 아니고 skin 입니다. 보시면 js, html, css, result 이렇게 네 가지 탭이 있는데 표시하고 싶은 것만 순서대로 나열하면 됩니다.

### Gist

Gist 는 Github 에서 제공하는 서비스 중 하나로, 간단한 코드를 작성해서 공유할 수 있는 서비스입니다. 파일 이름, 코드, 코드 설명만 작성하면 바로 파일이 만들어지고 공유할 수 있습니다.

```
{% gist gist_id [filename] %}
```

> gist_id: Gist 에서 코드를 생성한 후에 공유를 누르면 나오는 url 의 아이디 부분을 복사해서 넣으면 됩니다.

### iframe

아이프레임 (iframe) <iframe> 은 내부 프레임(inline frame) 으로 HTML 문서 내에서 다른 HTML 을 표시하는 태그입니다.

```
{% iframe url [width] [height] %}
```

### 내장 코드 삽입

source/downloads/code 폴더 상에 있는 코드를 포스트에 삽입할 수 있습니다.
```
% include_code [title] [lang:language] path/to/file %}
```

### 유튜브 (YouTube)

유튜브 비디오를 비디오 아이디만 있으면 바로 삽입 가능합니다.
```
{% youtube video_id %}
```
> video_id: 유튜브에서 비디오 공유를 눌러서 나오는 url 의 뒷 부분이 해당 비디오의 고유한 아이디입니다.

### Vimeo

비메오 (Vimeo) 도 유튜브와 동일하게 삽입 가능합니다.

```
{% vimeo video_id %}
```

> video_id: 비메오에서 비디오 공유를 눌러서 나오는 url 의 뒷 부분이 해당 비디오의 고유한 아이디입니다.

### 포스트 삽입

해당 블로그 내에 있는 포스트를 첨부할 수 있습니다. 굉장히 유용한 기능이네요! 따로 주소 복사해서 링크 만들지 않아도 됩니다.

```
{% post_path slug %}
{% post_link slug [title] %}
```

* slug: slug 는 포스트의 제목을 말합니다. Hexo 에서는 파일 제목이 url 이 되므로 포스트 파일 만들 때 사용한 파일명을 입력하면 됩니다.
* post_path: 포스트 제목을 입력하면 해당 포스트의 경로가 표시됩니다.
* post_link: 포스트 제목을 입력하면 해당 포스트의 링크가 생성됩니다.

### 자원 (Asset) 삽입

자원을 삽입하는 방법입니다. 이건 자원 폴더 (Asset folder) 와 관련이 있습니다. 해당 내용은 다음 포스트에서 다루도록 하겠습니다.

```
{% asset_path slug %}
{% asset_img slug [title] %}
{% asset_link slug [title] %}
```