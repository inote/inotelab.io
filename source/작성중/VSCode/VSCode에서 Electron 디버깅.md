# Visual Stdio Code에서 Electron 디버깅

참조 : [https://github.com/Microsoft/vscode-recipes/tree/master/Electron](https://github.com/Microsoft/vscode-recipes/tree/master/Electron)

Electron 1.7버전 - v8의 디버깅 프로토콜 inspector 지원 (node.js 6.3에서 추가). 이는 inspector를 이용해서 Electron의 메인 프로세스를 추적할 수 있다. 또한 Chromium과 동일한 프로토콜을 사용하기 때문에 Chrome의 개발자 도구를 그대로 이용할 수 있다.

VS Code의 디버거는 Multi-target debugging을 지원하는데, 이는 원래 서버와 클라이언트 코드를 동시에 디버깅 하는 경우를 위해 만들어진 기능이다. Electron의 경우 이 기능을 이용하면 메인 프로세스와 렌더러 프로세스를 동시에 디버깅이 가능하다.

먼저 메인 프로세스를 실행하는 디버깅 구성을 추가한다.

```json
{
   "type":"node",
   "request":"launch",
   "name":"Electron Main",
   "runtimeExecutable":"${workspaceRoot}/node_modules/.bin/electron",
   "args":[
      "${workspaceRoot}/main.js",
      "--remote-debugging-port=9333" //렌더러 프로세스의 디버깅 포트를 설정
   ],
   "protocol":"inspector" //v8 inspector 프로토콜을 사용하도록 지정
}
```

실행파일을 Electron으로 지정하는 것과 inspector 프로코콜을 사용하도록 지정하는 것, 그리고 렌더러 프로세스의 디버깅이 가능하도록 명령어를 추가하는 것 정도가 기본 구성과 다른 점이다.

다음으로는 렌더러 프로세스를 위한 구성을 추가한다.

```json
{
   "type":"node",
   "request":"attach",
   "name":"Electron Renderer",
   "protocol":"inspector",
   "port":9333
}
```

주의할 부분은 렌더러 프로세스도 여전히 node.js 디버거를 사용한다는 점이다. VS Code에는 Chrome 디버깅용 확장이 출시되어 있는데, 이 확장은 각 디버거를 따로 실행할 때는 문제가 없으나 Multi-target 디버깅을 위해 메인 프로세스 실행과 렌더러 프로세스 디버깅을 동시에 실행하면 렌더러 프로세스에 디버거가 접속을 하지 못하는 문제가 있다. 내부적으로는 두 디버거 확장이 모두 같은 코어를 사용하기 때문에 기능상의 차이는 없다.

마지막으로 이 두 디버거를 동시에 실행할 수 있도록 compound 설정을 추가한 다음 이 디버깅 타겟을 실행하면 두 디버거를 동시에 실행하고 각각의 프로세스간 스택을 이동하면서 디버깅을 할 수 있게 된다.

```json
"compounds":[
   {
      "name":"Electron",
      "configurations":[
         "Electron Main",
         "Electron Renderer"
      ]
   }
]
```

동시에 프로세스 사이 스택 및 Breakpoint를 추적
물론 이 기능은 이전의 node.js 디버깅 프로토콜을 이용해서도 가능하지만 (1.7이전 버전의 경우 legacy 프로토콜을 이용) 새로운 프로토콜을 이용하면서 Electron-prebuilt-compile에서의 Source map 지원 등 이전의 프로토콜에서 지원하기 다소 힘들었던 부분을 특별한 설정 없이 이용할 수 있다는 장점이 있다.

```json
{
    "version": "0.2.0",
    "configurations": [{
            "name": "Electron: Main",
            "type": "node",
            "request": "launch",
            "cwd": "${workspaceRoot}",
            "runtimeExecutable": "${workspaceRoot}/node_modules/.bin/electron",
            "protocol": "inspector", //v8 inspector 프로토콜을 사용하도록 지정
            "runtimeArgs": [
                "--remote-debugging-port=9223", //렌더러 프로세스의 디버깅 포트를 설정
                "."
            ],
            "windows": {
                "runtimeExecutable": "${workspaceRoot}/node_modules/.bin/electron.cmd"
            },
            "args": ["."]
        },
        {
            "type": "chrome",
            "request": "attach",
            "name": "Electron: Renderer",
            "port": 9223,
            "webRoot": "${workspaceRoot}",
            "timeout": 30000,
        },
    ],
    "compounds": [{
        "name": "Electron: All",
        "configurations": [
            "Electron: Main",
            "Electron: Renderer"
        ]
    }]
}
```