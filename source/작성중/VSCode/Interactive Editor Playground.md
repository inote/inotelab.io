# Interactive Editor Playground

The core editor in VS Code is packed with features. This page highlights a number of them and lets you interactively try them out through the use of a number of embedded editors. For full details on the editor features for VS Code and more head over to our documentation.

VS 코드의 core editor에는 기능들이 가득합니다. 이 페이지에서는 여러 항목을 강조 표시하고 여러 임베디드 편집기를 사용하여 대화식으로 시도 할 수 있습니다. VS 코드의 편집기 기능에 대한 자세한 내용은 설명서를 참조하십시오.

* 다중 커서 편집 - 블록 선택, 모든 항목 선택, 추가 커서 추가 등
* IntelliSense - 코드 및 외부 모듈에 대한 코드 지원 및 매개 변수 제안을 얻습니다.
* 라인 액션 - 신속하게 코드를 재 배열 할 수 있습니다.
* 리팩토링 이름 바꾸기 - 코드 기반에서 심볼의 이름을 빠르게 바꿉니다.
* 추출을 통한 리팩터링 - 공통 코드를 신속하게 별도의 함수 또는 상수로 추출합니다.
* 서식 지정 - 내장 된 문서 및 선택 서식을 사용하여 코드를 멋지게 유지하십시오.
* 코드 접기 - 다른 영역을 접어서 코드의 가장 중요한 부분에 집중하십시오.
* 오류 및 경고 - 입력하는 동안 오류 및 경고를 참조하십시오.
* 스니펫 - 스니펫으로 시간을 절약하십시오.
* Emmet - 통합 Emmet 지원은 HTML 및 CSS 편집을 한 차원 높여줍니다.
* JavaScript Type Checking - 구성이 0 인 TypeScript를 사용하여 JavaScript 파일의 유형 검사를 수행합니다.

## 다중 커서 편집

여러 커서를 사용하면 한 번에 문서의 여러 부분을 편집 할 수 있으므로 생산성이 크게 향상됩니다. 아래 코드 블록에서 다음 작업을 시도하십시오.

상자 선택 - 텍스트 블록을 선택하려면 Ctrl + <Shift> + Alt + DownArrow, Ctrl + <Shift> + Alt + RightArrow, Ctrl + <Shift> + Alt + UpArrow, Ctrl + <Shift> + Alt + LeftArrow Shift + Alt를 눌러 마우스로 텍스트를 선택할 수도 있습니다.
커서 추가 - Ctrl + Alt + UpArrow 또는 Ctrl + Alt + DownArrow를 눌러 새 커서를 위 또는 아래에 추가 할 수 있으며 Alt + 클릭으로 마우스를 사용하여 아무 곳이나 커서를 추가 할 수 있습니다.
모든 문자열 결과에 커서 생성 - 문자열 인스턴스 하나를 선택하십시오. 배경색을 선택하고 Ctrl + <Shift> + L을 누릅니다. 이제 간단히 입력하여 모든 인스턴스를 바꿀 수 있습니다.
멀티 커서 편집을위한 빙산의 일각은 선택 메뉴와 추가 작업을위한 편리한 키보드 참조 안내서입니다.

## JavaScript 및 TypeScript 리펙토링(refactoring)
