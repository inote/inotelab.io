# git 원격저장소 여러개 사용하기

git에서 remote repository를 여러개 사용하는 방법은 다음과 같다.

1. 로컬 저장소 디렉토리로 이동하여 커맨드 창을 연다.
```
cd existing_folder
```

2. 커맨드 창에서 다음 명령 입력.
```
git remote add [단축이름] [url]
예) git remote add origin git@gitlab.com:아이디/프로젝트.git
```

3. 원격 저장소 확인.
'git remote' 명령으로 현재 프로젝트에 등록된 원격 저장소를 확인할 수 있다. 좀 더 자세한 정보를 보려면 'git remote -v'를 입력하면 된다.
```
git remote
git remote -v
```

## 에러 해결

기존 원격저장소에 데이터가 있고 로컬저장소에도 데이터가 있는 상황에서 원격저장소를 추가하면, 새로운 원격저장소 push 상황에서 rejected 에러가 발생 할 수 있다. 이는 기존 데이터가 손실 될 수 있어서 진행을 안 되게 하는 것이다.
```
 ! [rejected]        master -> master (fetch first)
```

추가 한 원격 저장소를 로컬 저장소의 데이터와 동기화를 해야하는데 로컬 저장소 데이터로 덮어쓰는 것을 통해 에러를 제거할 수 있다. 하지만 기존 데이터 손실을 감안하고 강제로 push를 진행하는 것이기 때문에 데이터 손실에 유의해야한다. 다음의 명령을 입력.
```
git push -f origin master
or
git push +origin master
```

gitlab의 경우 강제 push가 안 될 수 있다. 그럴 경우 프로젝트의 Settings > Repository > Protected Branches 항목에서 Unprotected 버튼을 누르고 push를 재실행 하면 된다.

gitlab에서 `! [remote rejected] master -> master (pre-receive hook declined) ` 에러 코드가 내려오면 gitlab에서 소스 프로젝트->세팅->protected branches에 developer can push라는 체크박스
 체크하고 unprotect 버튼 클릭하면 강제로 덮어쓸수 있음.

PULL과 PUSH에서 브렌치 체크아웃 에러가 나는 경우
conflict 부분을 해결하고 다음 명령으로 브랜치를 전환한다.
```
git checkout (브랜치)
```

3. 확인
'git remote' 명령으로 현재 프로젝트에 등록된 리모트 저장소를 확인해 본다. 좀 더 자세한 정보를 보려면 'git remote -v'를 입력하면 된다. 그런데 gitlab에서 `! [remote rejected] master -> master (pre-receive hook declined) ` 에러 코드가 내려오면 gitlab에서 소스 프로젝트->세팅->protected branches에 developer can push라는 체크박스
 체크하고 unprotect 버튼 클릭하면 강제로 덮어쓸수 있음.


> [참조 : Git의 기초 - 리모트 저장소](https://git-scm.com/book/ko/v1/Git%EC%9D%98-%EA%B8%B0%EC%B4%88-%EB%A6%AC%EB%AA%A8%ED%8A%B8-%EC%A0%80%EC%9E%A5%EC%86%8C)

```
cd existing_folder
git init
git remote add [origin or 원격저장소 이름] git@gitlab.com:jasperkim/ameba.git
git add .
git commit -m "Initial commit"
git push -u [origin or 원격저장소 이름] master
```