---
title: git reset과 revert 차이
tags: Git
categories:
  - Git
---
### git reset, revert 차이점

git에서 이력을 되돌리는 방법은 `git reset`과 `git revert`가 있습니다. 둘의 차이점은 reset은 해당 커밋 이후를 모두 삭제하고 이전 상태로 되돌아 가는 것이고 revert는 이전 상태는 갖고있는 상태에서 작업을 이전으로 되돌리는 걸 말합니다. 백투더 퓨쳐 처럼 과거로 되돌아 가는가 아니면 과거의 잘못을 고치는가의 차이입니다.

- 참조 : [git reset 문서](https://git-scm.com/book/ko/v2/Git-%EB%8F%84%EA%B5%AC-Reset-%EB%AA%85%ED%99%95%ED%9E%88-%EC%95%8C%EA%B3%A0-%EA%B0%80%EA%B8%B0)

### git reset 사용법

옵션에는 hard, mixed, soft 3가지가 있습니다. 기본은 mixed입니다. hard는 이력 이후 내용을 지웁니다. soft는 head가 해당 이력으로 돌아가지만 이후 내용은 남아있습니다. mixed는 이력을 되돌리고 인덱스를 초기화 합니다. 하지만 변경내용은 남아있는 상태입니다. 스테이징에 올려져 있지는 않지만 변경내용을 갖고 있어서 다시 커밋을 할 수 있는 상태이죠.

돌아가고 싶은 커밋을 지정하려면 커밋 해쉬를 입력해서 지정해주면 됩니다. 또는 현재 HEAD에서 3개 전으로 돌아가기 위해서 `git reset HEAD~3`으로 상대적으로 지정해도 됩니다.

> 여기서 `HEAD`는 현재 브랜치를 가리키는 포인터입니다. 또한 `staging area`는 **바로 다음에 커밋할 것들**을 말합니다. 즉 `git commit` 했을 때 처리 할 것들이 있는 곳입니다.

```Bash
$ git reset <옵션> <돌아가고 싶은 커밋>
$ git reset hard
$ git reset soft
$ git reset HEAD~3
```

### reset 심화학습

reset 명령은 정해진 순서대로 Head, Index, Working Directory 세 개의 트리를 덮어써 나가다가 옵션에 따라 지정한 곳에서 멈춥니다.

1. HEAD가 가리키는 브랜치를 옮긴다. (--soft 옵션이 붙으면 여기까지)
2. Index를 HEAD가 가리키는 상태로 만든다. (--hard 옵션이 붙지 않았으면 여기까지)
3. 워킹 디렉토리를 Index의 상태로 만든다.

### git revert 사용법
특정 이력으로 지정하던가 `...`으로 범위를 지정할 수 있습니다. 1234567와 1234500 사이의 이력을 모두 되돌리려면 `1234567..1234500`로 입력합니다.

```Bash
$ git revert <되돌릴 커밋>
$ git revert HEAD~3
$ git revert 1234567..1234500
```

### 원격 저장소의 commit 되돌리기

remote repository의 commit을 되돌리려면 `git checktou <돌아가고 싶은 커밋>`을 입력합니다.

### 정리

reset은 이력을 과거로 되돌리고 revert는 과거 이력으로 업데이트한다.

> 글이 이해가 어려운 분들은 다른 블로그 글인 [만화로 설명된 reset과 revert의 차이점](http://devpools.kr/2017/01/31/%EA%B0%9C%EB%B0%9C%EB%B0%94%EB%B3%B4%EB%93%A4-1%ED%99%94-git-back-to-the-future/)에 잘 설명되어 있습니다.
