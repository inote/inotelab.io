---
title: GitLab 마크다운 규칙
date: 2017-11-17 24:00:00
tags: GitLab
categories:
  - GitLab
---

마크 다운으로 블로그 글을 작성해서 올렸는데 리스트의 목록 사이의 간격이 벌어지는 오류가 발생했습니다.
원인을 찾아보니 원래는 <ol> 태그만 들어가야 하는데 그 사이에 <p> 태그가 삽입이 되네요.
간격이 벌어지는 부분들의 공통점을 확인하니 '''이 들어가면서 줄바꿈이 더 들어가서 <p> 태그로 인식되게 되었나 봅니다.
코드 삽입시 유의 해야겠습니다.

## 코드 삽입 시

` ``` ` 다음에 코드 언어를 입력하면 해당 언어 색상을 좀 더 예쁘게 표시해줍니다.

MD040 - Fenced code blocks should have a language specified

Tags: code, language

Aliases: fenced-code-language
This rule is triggered when fenced code blocks are used, but a language isn't specified:


```javascript
#!/bin/bash
echo Hello world
```
To fix this, add a language specifier to the code block:

```bash
#!/bin/bash
echo Hello world
```

[깃랩 마크다운 문서](https://docs.gitlab.com/ee/user/markdown.html)
[깃랩 마크다운 문서](https://gitlab.com/help/user/markdown.md)
[깃랩 마크다운 문서](https://github.com/DavidAnson/markdownlint/blob/v0.6.3/doc/Rules.md#md040)