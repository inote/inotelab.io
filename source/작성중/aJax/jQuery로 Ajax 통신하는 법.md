---
title: jQuery로 Ajax 통신하는 법
date: 2017-11-17 24:00:00
tags: Web
categories:
  - aJax
---

jQuery로 Ajax 통신을 하면 크로스브라우징의 문제를 해결합니다.
브라우저들간의 차이점을 jQuery가 알아서 해결해 줍니다. 동일한 코드를 갖게 된다.
jQuery의 Ajax기능
> 크로스브라우징(Cross Browsing, 웹페이지의 상호 호환성) : 표준 웹기술을 이용해서 내가 만든 웹페이지가 다른 기종이나 플랫폼 등 모든 브라우저에서 깨짐 없이 의도한 대로 구현되게 어느 한쪽에 치우치지 않도록 공통요소로 웹페이지를 제작하는 작업 또는 기법을 말합니다.

### 참조
* [생활코딩 강좌](https://opentutorials.org/course/1375/6851)
* [제이쿼리 Ajax API 문서](http://api.jquery.com/category/ajax/)