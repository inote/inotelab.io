---
title: GitLab 블로그 구글 애널리틱스(Google Analytics) 등록하기
date: 2017-11-17 24:00:00
tags: GitLab
categories:
  - GitLab
---

블로그를 만들었다면 블로그 방문자에 대한 분석의 필요성을 느끼게 됩니다.
이때 좋은 도구가 구글 애널리틱스입니다. 구글 애널리틱스는 웹사이트 방문 현황과 통계를 분석할 수 있는 툴입니다.

이번 글에서는 [구글 애널리틱스(Google Analytics)](https://analytics.google.com/)에 등록하는 방법을 알아보겠습니다.


Hexo의 hueman 테마를 쓰고 있다면 테마 폴더의 _config.yml 파일에서 손쉽게 구글 애널리틱스 항목에 추적 ID를 넣으면 됩니다.
```
google_analytics: UA-12345678-1 # enter the tracking ID for your Google Analytics
```
