
electron 샘플을 실행하기 위해 `npm install electron`을 실행했는데 인스톨 도중 멈춤(hang up, got stuck on) 현상이 발생하였습니다. 몇 분을 기다려도 똑같은 화면인데요. 해결 방법을 찾았습니다.

```shell
> electron@1.4.15 postinstall /Volumes/Slave/scrap/electron-quick-start/node_modules/electron
> node install.js
```

참조 : [https://github.com/electron/electron-quick-start/issues/127](https://github.com/electron/electron-quick-start/issues/127)

아래와 같이 커맨드 명령을 하면 됩니다. `npm uninstall`은 설치된 패키지를 제거합니다. `npm install`에 `--verbose` 옵션을 붙이는 이유는 npm 설치에 문제가있는 경우 자세한 정보를 얻기 위해서입니다.

```
$ npm uninstall -g electron
$ npm install -g --verbose electron
```