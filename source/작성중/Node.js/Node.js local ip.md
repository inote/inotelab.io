---
title: Node.js local ip
tags: Node.js
categories:
  - Node.js
---

> 내 PC의 무선 IP 주소를 알려면 어떻게 해야할까? Get local ip address in node.

### wifi-control

현재 무선인터페이스 정보를 가져오기 위해서 'wifi-control' 모듈을 설치합니다.

### Find Wireless Interface
무선 카드가 자주 변경되거나 켜지거나 꺼져있는 경우가 아니면 이 방법을 자주 사용할 필요가 없습니다.

인수없이 호출하면 `WiFiControl.findInterface()`는 호스트 시스템에서 유효한 무선 인터페이스를 자동으로 찾습니다.

문자열 인수 인터페이스가 제공되면 이 값은 호스트 시스템의 의도 된 무선 인터페이스로 사용됩니다. 다양한 운영 체제의 일반적인 값은 다음과 같습니다.

os	| typical values
----|----------------
Linux	| wlan0, wlan1, ...
Windows	| wlan
MacOS	| en0, en1, ...

Example:
```
var resultsAutomatic = WiFiControl.findInterface();
var resultsManual = WiFiControl.findInterface( 'wlan2' );
```
output
```
resultsAutomatic = {
    "success":  true,
    "msg":  "Automatically located wireless interface wlan2.",
    "interface":  "wlan2"
  }
  resultsManual = {
    "success":  true,
    "msg":  "Wireless interface manually set to wlan2.",
    "interface":  "wlan2"
  }
```

### Find Local IP

글쎄요, 다른 종류의 이유 때문에 로컬 IP 주소가 무엇인지 알기를 원할 수 있습니다. 그래서 다른 서비스에 브로드 캐스트 할 수 있습니다.

```
var localip = require('local-ip');
var iface = 'wlan0';

localip(iface, function(err, res) {
  if (err) {
    throw new Error('I have no idea what my local ip is.');
  }
  console.log('My local ip address on ' + iface + ' is ' + res);
});
```
or
```
var iface = 'wlan0';
var localip = require('local-ip')(interface);
console.log('My local ip address on ' + iface + ' is ' + localip);
```

인터페이스는 wlan0과 같은 네트워크 인터페이스 여야합니다. 콜백은 선택 사항이며 지정된 경우 서명 콜백 (err, res)과 함께 호출됩니다. 콜백을 지정하지 않으면 값이 직접 반환되거나 오류가 발생하면 오류가 발생합니다.



```
 var ifaceState = WiFiControl.getIfaceState();
```

로컬 WiFi 액세스 포인트를 검색하고 네트워크에 연결 / 연결 해제하는 방법을 제공하는 NodeJS 모듈입니다. Windows, Linux 및 MacOS에서 작동합니다.

어쩌면 당신은 SoftAP 기반의 IoT 장난감을 가지고있을 것입니다. 그리고 당신은 thin downloadeable "셋업" 클라이언트를 만들 필요가 있습니까? AP를 자주 변경해야하는 헤드리스 Rpi 기반 장치를 만들고 싶습니까?

> Q : Rpi-based device란?

```
var WiFiControl = require('wifi-control');

  //  Initialize wifi-control package with verbose output
  WiFiControl.init({
    debug: true
  });

  //  Try scanning for access points:
  WiFiControl.scanForWiFi( function(err, response) {
    if (err) console.log(err);
    console.log(response);
  });
```

다음 방법은 Windows, MacOS 및 Linux에서 동일한 기능을 제공하기 위해 서로 다른 명령을 사용합니다. 광범위한 스트로크에서 우리가 활용하고있는 기본 시스템 명령은 다음과 같습니다.

But You may encounter errors if you use this module on a system lacking these commands!

 os	| command
--------|----------
Windows	| netsh
MacOS	| networksetup
Linux	| nmcli

### Synchronicity에 대한 참고 사항

일부 WiFiControl 메소드는 동기식이며 일부는 비동기식입니다.

동기식

* WiFiControl.init( settings )
* WiFiControl.configure( settings )
* WiFiControl.findInterface( iface )
* WiFiControl.getIfaceState()

비동기식

* WiFiControl.scanForWiFi(callback ) - 콜백을 사용하여 검색 결과를 반환하기까지 시간이 오래 걸릴 수 있습니다 (1 ~ 10 초).
* WiFiControl.connectToAP(ap, callback) -이 작업은 몇 분 정도 걸릴 수 있으므로 콜백을 사용하여 이동 경로를보고합니다.
* WiFiControl.resetWiFi(callback ) - 무선 카드에 따라 전원을 껐다가 다시 켤 때까지 시간이 걸릴 수 있으므로이 메서드는 콜백이 완료되면 콜백을 반환합니다.

Initialize

  ```
  WiFiControl.init( settings );
  ```
WiFiControl이 호스트 컴퓨터의 무선 인터페이스를 사용하여 스캔하거나 연결 / 연결 해제하기 전에 무선 인터페이스가 무엇인지 알아야합니다!

네트워크 인터페이스를 초기화하고 동시에 모든 사용자 정의 설정을 전달하려면 시작시 서버의 `WiFiControl.init( settings )`을 호출하기만 하면 됩니다. `settings`은 선택적 매개 변수입니다 (아래 `WiFiControl.configure (settings)` 참조).

똑같이 수동으로 WiFiControl 모듈에 무선 인터페이스 위치를 지정하거나 네트워크 인터페이스를 수동으로 강제 설정하려면 `WiFiControl.findInterface (iface)` 명령을 참조하십시오.


참조 :
* [npm wifi-control](https://www.npmjs.com/package/wifi-control)
* [npm local-ip](https://www.npmjs.com/package/local-ip)

