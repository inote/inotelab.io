# Become a UI Designer

UI디자이너가 되기 위해서는 다음의 작업들이 기본적으로 필요합니다.

* Wireframes
* Prototype
* Visual Design
* Interaction Design
* 그외 기타

## Wireframes

기본 뼈대를 세우고

[5 Free Quick Wireframe Tools For UI/UX Designers in 2017](https://uxplanet.org/5-free-quick-wireframe-tools-for-ui-ux-designers-in-2017-189e6a594fda)

## Prototype

아이디어의 프로토타입을 만듭니다.

[Top 20 Prototyping Tools For UI And UX Designers 2017](https://blog.prototypr.io/top-20-prototyping-tools-for-ui-and-ux-designers-2017-46d59be0b3a9)

## Visual Design

그리고 PhotoShop이나 AI등으로 실제 디자인을 하죠.

[2017 Design Trends Guide](https://www.behance.net/gallery/47810259/2017-Design-Trends-Guide?utm_content=buffer32ccb)

## Interaction Design

각각의 디자인된 결과물은 Adobe After Effect로 UI Motion 작업 후 UI Interactive Prototype은 Flinto등의 툴을 사용하는군요.

[Valuable Interaction Design Tools](https://www.hyperisland.com/community/news/7-valuable-interaction-design-tools-reviewed)

[Flinto – The App Design App](https://www.google.co.kr/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0ahUKEwjA5sra0dbXAhXFybwKHVtyACoQFgglMAA&url=https%3A%2F%2Fwww.flinto.com%2F&usg=AOvVaw1rJ_vIp0lcA6mBEV7kMEpj)
