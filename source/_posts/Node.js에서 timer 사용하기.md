---
title: Node.js에서 timer 사용하기
date: 2017-12-05 24:00:00
tags: 
  - Node.js
  - Javascript
categories:
  - Node.js
---

Node.js 즉 javascript에서 c언어의 timer와 같은 기능을 사용하는 방법입니다.

### setTimeout

지정된 시간 이후에 입력한 함수를 호출합니다. 지정된 시간 이 후 한번만 호출되기 때문에 반복적인 실행이 필요한 경우 `setInterval()` 메소드를 사용합니다. 만약 중도에 호출을 취소하고 싶은 경우는 `clearTimeout` 메소드를 호출합니다. `clearTimeout` 메소드를 호출할 때 파라미터로 `setTimeout`의 ID 값이 필요한데 이 값은 `setTimeout`의 반환 값을 사용합니다.

```javascript
setTimeout(function(){ consol.log("hi"); }, 2000);
```

### clearTimeout

`clearTimeout` 메소드는 `setTimeout`로 설정한 타이머가 호출되기 전에 이를 클리어합니다.

```javascript
var myTimeout = setTimeout(function(){ consol.log("hi"); }, 2000);
clearTimeout(myTimeout);
```

### setInterval

`setInterval` 메소드는 지정된 시간 간격만큼 반복해서 주어진 함수를 호출합니다. 만약 반복적인 호출을 중도에 멈추고 싶은 경우는 `clearInterval` 메소드를 호출합니다. `clearInterval` 메소드를 호출할 때 파라미터로 `setInterval`의 ID 값이 필요한데 이 값은 `setInterval`의 반환 값을 사용합니다.

```javascript
setInterval(function(){ consol.log("hi"); }, 2000);
```

### clearInterval

`clearInterval` 메소드는 `setInterval`로 설정한 타이머를 클리어합니다.

```
var myTimer = setInterval(function(){ consol.log("hi"); }, 2000);
clearInterval(myTimer);
```

### 정리

Node.js에서 즉 javascript의 타이머 사용은 한번 실행은 `setTimeout`과 `clearTimeout`의 쌍을 사용하고 반복적인 실행은 `setInterval`과 `clearInterval`을 사용하면 됩니다.