---
title: Electron EXE File 만드는 방법
date: 2018-01-23 24:00:00
tags:
  - Node.js
  - Electron
categories:
  - Node.js
---

# 개요
Electron으로 실행파일을 만드는 방법.

# 참조
* [https://proinlab.com/archives/1928](https://proinlab.com/archives/1928)
* [https://www.christianengvall.se/electron-packager-tutorial/](https://www.christianengvall.se/electron-packager-tutorial/)

```
$ npm install electron-packager --save-dev # npm 스크립트로 사용
$ npm install electron-packager -g # 전역 모듈로 설치

electron-packager ./ myApp --platform=win32 --arch x64 --out release --prune

electron-packager ./ myApp --platform=win32 --arch x64 --out release --prune --icon assets/icons/win/icon.ico


electron-packager . electron-tutorial-app --overwrite --asar=true --platform=win32 --arch=x64 --icon=assets/icons/win/icon.ico --prune=true --out=release-builds --version-string.CompanyName=CE --version-string.FileDescription=CE --version-string.ProductName="Electron Tutorial App"



electron-packager . easyApp --platform=win32 --arch=x64 --out=release --prune=true --overwrite --icon=assets/icons/win/icon.ico


electron-packager ./ easyApp --platform=win32 --arch x64 --out release --prune true --icon assets/icons/win/icon.ico

```