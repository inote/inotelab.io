---
title: Electron App Icon 변경하기
date: 2017-12-07 24:00:00
tags:
  - Node.js
  - Electron
categories:
  - Node.js
---

Electron 애플리케이션의 아이콘을 변경하는 방법입니다.

### 1. 아이콘과 아이콘 파일들 만들기

제일 먼저 해야 할 일은 1024x1024 픽셀 크기의 아이콘을 만드는 것입니다. 다 만들어졌다면 PNG 포맷으로 저장합니다.

이제 [ICONVERT ICONS](https://iconverticons.com/online/) 사이트로 이동합니다. 이 사이트에 PNG 파일을 업로드하면 다른 아이콘 포맷들을 만들어 줍니다.

### 2. 폴더 구조 만들기

아래 처럼 기본 폴더를 만듭니다. assets 폴더에 icons 폴더를 만듭니다. 그리고 그 안에 mac, png, win 폴더를 만듭니다. 각각의 mac 폴더에는 `icon.icns`, png 폴더에는 `*.png`, win 폴더에는 `icon.ico` 파일을 넣습니다.

```
.
└── assets
    └── icons
        ├── mac
        ├── png
        └── win
```

### 3. main.js의 아이콘 패스 업데이트

main.js 파일을 열어서 `BrowserWindow` 초기화 시에 `icon: path.join(__dirname, 'assets/icons/png/64x64.png')` 코드를 넣습니다.

```javascript
const electron = require('electron')
var path = require('path')

...

 mainWindow = new BrowserWindow({titleBarStyle: 'hidden',
     width: 1281,
     height: 800,
     minWidth: 1281,
     minHeight: 800,
     backgroundColor: '#312450',
     show: false,
     icon: path.join(__dirname, 'assets/icons/png/64x64.png')
 })
```

참조 원문 :

* [Github Electron tutorial app](https://github.com/crilleengvall/electron-tutorial-app)
* [Electron app icons](https://www.christianengvall.se/electron-app-icons/)

