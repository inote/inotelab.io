---
title: Node.js WiFi Refresh
date: 2017-11-22 24:00:00
tags: 
  - Node.js
  - Web
categories:
  - Node.js
---

무선 와이파이 리스트 갱신이 필요한 경우 커맨드 창에서 `netsh wlan refresh`를 입력하면 됩니다. 같은 기능을 Node.js에서 하려면 child_process 모듈을 쓰면 됩니다.

[자세한 사항은 아래 링크를 참조하세요]
* 영문 - [https://nodejs.org/api/child_process.html#child_process_child_process](https://nodejs.org/api/child_process.html#child_process_child_process)
* 한글 - [http://nodejs.sideeffect.kr/docs/v0.8.15/api/child_process.html](http://nodejs.sideeffect.kr/docs/v0.8.15/api/child_process.html)
* 코드참조 - [https://gist.github.com/nanha/3671768](https://gist.github.com/nanha/3671768)

## child_process

child_process 모듈은 자식 프로세스 생성 기능을 제공하는 모듈입니다. child_process.spawn() 함수를 호출하면 자식 프로세스가 생성됩니다. 사용법은 다음과 같습니다.

작업 폴더에 subprocess.js란 파일 모듈을 만듭니다.

> 모듈이란? 따로 분리해야 할 독립적인 기능을 별도의 파일로 만든걸 말합니다. 이렇게 하는 이유는 하나의 함수에 모든 기능을 넣는 것 보다 기능별로 나누어 놓으면 필요할 때 다른 곳에서도 재활용할 수 있기 때문입니다. Node.js에서는 이렇게 분리된 파일을 모듈이라 부릅니다. 모듈을 불러와서 사용하는 방법에 관한 정의는 [CommonJs](https://nodejs.org/docs/latest/api/modules.html)의 표준 스펙을 따릅니다.

```JavaScript
var spawn = require('child_process').spawn;

function subprocess(processname, arg, cb) {
  var p = spawn(processname, arg);
  p.on('exit', cb.exit);
  p.stdout.on('data', cb.stdout || function (out) {
    process.stdout.write(out);
  });
  p.stderr.on('data', cb.stderr || function (err) {
    process.stdout.write(err);
  });
};

module.exports = subprocess;
```

subprocess 모듈을 사용자 애플리케이션에서 호출하여 사용합니다. subprocess() 함수의 인자로는 프로세스 이름과 파라미터 속성이 들어있는 배열을 넣으면 됩니다. 가령 'ls -al'을 사용할 경우. 'subprocess("test", p, ["ls","-al"]);'와 같이 호출합니다.

```JavaScript
var subprocess = require('./subprocess'),
    util = require('util');

var processname = 'netsh';
var p = [];
p.push('netsh');
p.push('wlan');
p.push('refresh');

subprocess(processname, p, {
    stdout: function(out) {},
    stderr: function(out) {},
    exit: function(code) {}
});
```