---
title: GitLab Pages에 Hexo 블로그 설치하기
date: 2017-11-16 24:00:00
tags: GitLab
categories:
  - GitLab
---

**마크다운(Markdown)** 언어의 편리함을 블로그에 적용할 수 있을까 하고 구글링 하던 중 github에서 github pages라는 블로그 서비스를 이용할 수 있음을 알게 되었습니다.

이전부터 git은 쓰고있던 터라 부담없이 해봤는데 영어라 쉽지 않네요. 먼저 해본 분들이 있지 않을까 하고 찾아봤는데, 역시나 있군요. github pages를 구축하려 한다면 Eric Han님의 [워드프레스보다 쉬운 Hexo 블로그 시작하기](http://futurecreator.github.io/2016/06/14/get-started-with-hexo/) 블로그 글을 추천합니다. 저도 이 글 따라서 github pages를 구축했습니다.

github pages에 조금 아쉬운 감이 있어 현재 사용중인 GitLab에서도 되지 않을까 싶어 해봤는데 비슷한 GitLab pages가 있더군요. 이 글은 gitlab pages에 Hexo 프레임워크를 적용하는 방법입니다.

> [Hexo](https://hexo.io/)는 심플하고 빠른 블로그 프레임워크입니다.

## GitLab Pages
GitLab에서 제공하는 정적(Static) 웹사이트입니다. 정적 웹사이트란 웹 페이지가 한번 만들어지고 나면 바뀌지 않는 고정된 웹페이지를 말합니다. 이와같은 정적 사이트를 만드는 방법에는 HTML, CSS 및 자바스크립트를 사용해서 직접 코딩하거나 [정적 사이트 생성기(SSG, Static Site Generator)](https://www.staticgen.com/)를 사용하여 만드는 방법이 있습니다. 그 중 GitLab Pages는 SSG를 사용합니다.

> [gitlab pages 문서](https://docs.gitlab.com/ee/user/project/pages/index.html)

## 깃랩 페이지에 블로그 설치하기

[GitLab 페이지](https://www.gitlab.com/)에서 빈 프로젝트를 만들어서 Hexo를 시작하는 것 보다 이미 만들어져 있는 프로젝트를 fork하는게 훨씬 간단해서 이 방법을 선택하였습니다. fork 한 뒤에 설정만 일부 변경하면 gitlab pages가 뙇 하고 생기거든요.

> 만약 새로운 Repository를 만드는 것 부터 시작하실 분들은 Repository 이름을 지을 때 주의할 점이 있습니다. `username.gitlab.io`라고 이름을 지으면 사용자의 GitLab Page를 만드는 것이고 따라서 블로그 주소도 `username.github.io`가 됩니다. 하지만 다른 이름으로 지으면 `username.github.io/repository_name`으로 웹페이지 주소가 설정됩니다. 주소가 루트가 아니라 서브로 따라붙습니다. 페이지의 성격에 따라서 정하도록 합니다. 

우선 링크의 [목록](https://gitlab.com/pages)들 중에서 Hexo를 fork하여 프로젝트를 가져 옵니다.

fork가 잘 되었다면 내 프로젝트 목록에 hexo 프로젝트가 생겼을 겁니다. 프로젝트의 `Settings > General > Advanced settings > Remove fork relationship`에서 `fork relationship`을 제거합니다.

다음으로 포크의 `Shared Runners`가 활성화 되어있는지 확인합니다. 안 되어 있으면 사용하도록 합니다. 프로젝트의 `Settings > CI/CD > Runners settings > Shared Runners` 경로에서 확인하세요.

사용자 페이지를 만들기 위해서 프로젝트 이름을 변경합니다. 프로젝트 이름은 `username.gitlab.io` 이런 식으로 지어주셔야 합니다. 제 경우는 `inote.gitlab.io`가 되겠군요.
```
Settings > General > Advanced settings > Rename repository
```

`Repository`의 `_config.yml` 파일을 수정합니다. 자세한 부분은 나중에 수정하기로 하고 일단 root 부분을 자신의 웹사이트 주소로 수정합니다.
```
# URL
url: /
root: "/username.gitlab.io/"
```

자. 이제 나만의 gitlab pages가 만들어졌습니다. 수고하셨습니다. 

자신의 블로그 홈페이지 주소인 `username.gitlab.io` 웹페이지에 가보면 유명한 Hello World를 볼 수 있을겁니다. 웹 사이트 URL은 프로젝트의 `Settings > Pages`에서도 확인할 수 있습니다.

### 좀 더 필요한 것들 : 로컬에 Hexo 설치하기

GitLab에 만들어진 걸 git으로 clone해서 로컬에 가져올 텐데요. 리모트 올리지 않고 사용자 pc에서 웹페이지를 확인해보고 싶은 경우도 생길겁니다. 그럼 Hexo를 로컬에 설치해 주면 됩니다.

로컬 저장소 디렉토리에서 다음 커맨드를 입력합니다. 먼저 npm과 git이 설치되어 있어야함은 물론 당근이겠죠~
```
$ npm install -g hexo-cli
``` 

다음 명령을 입력하면 Hexo 기본 디렉토리와 파일을 생성합니다. 이 중 _config.yaml 파일은 프로젝트 설정 파일입니다. 블로그의 전반적인 셋팅은 이 파일을 조정하면 됩니다. 여러 속성들이 있지만 그 중 deploy는 Deployment 시에 원격 저장소 어디에 연결할지 정합니다.
```bash
$ hexo init blog
$ cd blog
$ npm install
```
기본 생성 디렉토리와 파일

```
.
├── _config.yml
├── package.json
├── scaffolds
├── source
|   ├── _drafts
|   └── _posts
└── themes
```

로컬서버 구동은 다음 명령어를 사용합니다. 명령 실행 후 브라우저에서 확인할 수 있습니다.
```
$ hexo server
```

이제 글을 작성해보죠. 다음 명령어는 포스트 마크다운 파일이 새로 생성되도록 합니다. `/source/_posts` 폴더 내에 마크다운 파일이 생성됩니다. 입력 형식에서 `[layout]`에는 post, page, draft 3가지 중 들어갑니다. 생략하면 post에 생성됩니다. `<title>`은 파일이름입니다.
```
$ hexo new [layout] <title>
```
layout  |	file path 
--------|-----------------
post	| source/_posts
page	| source
draft	| source/_drafts

해당 포스트 마크다운 파일을 열어보면 생성날짜와 제목이 아래와 같이 들어가 있습니다. 이와 같은 걸 `front-matter`라고 부릅니다.

```javascript
---
title: post name
date: 2017-xx-xx xx:xx:xx
---
```

생성과 배포 명령은 다음과 같습니다.

```javascript
$ hexo generate
$ hexo deploy

또는

$ hexo g -d
```

주의할 점은 이런식으로 Hexo를 로컬에 설치해서 글작성, 설정 변경 후 생성과 배포를 통하여 Gitlab Pages에 업로드가 되면 원격저장소의 자동화된 기능이 동작을 하지 않더군요. 